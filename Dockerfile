FROM python:3

# Mise à jour du répertoire des listes de paquets et des permissions en tant qu'utilisateur root
USER root
RUN mkdir -p /var/lib/apt/lists/partial \
    && chown -R 1000:1000 /var/lib/apt/lists \
    && chmod -R 755 /var/lib/apt/lists

# Créez un utilisateur non root et définissez un mot de passe
USER root
RUN groupadd -g 1000 app \
    && useradd -u 1000 -g app -m app \
    && echo 'app:shnr31161' | chpasswd

# Utilisez l'utilisateur créé
USER app

# Définissez le répertoire de travail dans le conteneur
WORKDIR /home/app

# Copiez votre script Python dans le conteneur
COPY . /home/app

# Installez les dépendances nécessaires, nettoyez le cache des paquets en tant qu'utilisateur root
USER root
RUN apt-get update \
    && apt-get install -y ansible \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Revenez à l'utilisateur non root pour l'installation des dépendances Python
USER app
RUN pip install tk pexpect pyyaml

# Commande à exécuter lors du démarrage du conteneur
CMD ["python3", "main.py"]
